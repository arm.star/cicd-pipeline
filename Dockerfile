FROM node:7.8.0
USER node
WORKDIR /opt
ADD . /opt
RUN npm install
ENTRYPOINT npm run start